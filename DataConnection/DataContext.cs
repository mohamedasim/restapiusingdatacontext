using Microsoft.EntityFrameworkCore;
using StudentApiTask.Models;

namespace StudentApiTask.DataConnection
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions <DataContext> options) : base(options){

        }
        public DbSet<StudentModel> Students{get;set;}
    }
}