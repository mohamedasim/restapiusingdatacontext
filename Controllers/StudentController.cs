using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StudentApiTask.DataConnection;
using StudentApiTask.Models;

namespace StudentApiTask.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentController:Controller
    {
        private DataContext _studentContext;
        public StudentController(DataContext context){
            _studentContext = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<StudentModel>> Get()
        {
           
          return _studentContext.Students.ToList();
        }        
        ~StudentController(){
            _studentContext.Dispose();
        }
    }
}